#include <stdint.h>

typedef uint8_t u8;
typedef int8_t s8;
typedef uint16_t u16;
typedef int16_t s16;
typedef uint32_t u32;
typedef int32_t s32;
typedef uint64_t u64;
typedef int64_t s64;

#define FUSE_USE_VERSION 30

#define _FILE_OFFSET_BITS 64

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <fuse.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>

#define LIST(typ,ptr,num)\
typ* ptr;\
s32 num;



void PrintToFile(s8* str) {
	FILE* fp = fopen("debug.txt","a");
	fwrite(str,strlen((char*)str),1,fp);
	fwrite("\n",1,1,fp);
	fclose(fp);
}



typedef struct _FileData_t FileData_t;
typedef struct _TagData_t TagData_t;

struct _TagData_t {
	s8* tag;
	LIST(FileData_t*,names,NameNum)
};

struct _FileData_t {
	s8* name;
	LIST(TagData_t*,tags,TagNum);
};

LIST(FileData_t,FileData,FileDataNum);
LIST(TagData_t*,TagData,TagDataNum);

s8* FilePath;
s8* ListPath;
s8* MountPath;



TagData_t* TagByString(s8* str) {
	for (s32 i = 0; i < TagDataNum; i++) {
		if (!strcmp((char*)TagData[i]->tag,(char*)str)) {
			return TagData[i];
		}
	}
	
	TagData_t* ret = calloc(1,sizeof(TagData_t));
	ret->tag = str;
	
	TagData = realloc(TagData,sizeof(TagData_t*)*(TagDataNum + 1));
	TagData[TagDataNum] = ret;
	TagDataNum++;
	
	return ret;
}

void ParseList(s8* data,s32 size) { //parsing plain txt files + tags list
	FileDataNum = 0;
	
	for (s32 i = 0; i < size; i++) {
		if (data[i] == 0xA) {
			data[i] = 0;
			if (i != 0 && data[i - 1] == 0) { //two newlines
				FileDataNum++;
			}
		}
	}
	FileDataNum++;
	
	FileData = calloc(FileDataNum,sizeof(FileData_t));
	
	s32 seek = 0;
	for (s32 i = 0; i < FileDataNum; i++) { //Filling FileDatas
		FileData[i].name = &data[seek];
		seek += strlen((char*)&data[seek]) + 1;
		while (strlen((char*)&data[seek]) != 0) {
			FileData_t* fd = &FileData[i];
			
			fd->tags = realloc(FileData[i].tags,sizeof(TagData_t*)*(FileData[i].TagNum + 1));
			fd->tags[fd->TagNum] = TagByString(&data[seek]); //Set Tag to FileData
			fd->tags[fd->TagNum]->names = realloc(fd->tags[fd->TagNum]->names,sizeof(FileData_t*)*(fd->tags[fd->TagNum]->NameNum + 1));
			fd->tags[fd->TagNum]->names[fd->tags[fd->TagNum]->NameNum] = fd; //Set FileData to Tag
			fd->tags[fd->TagNum]->NameNum++;
			fd->TagNum++;
			seek += strlen((char*)&data[seek]) + 1;
		}
		seek++;
	}
	
	/*for (s32 i = 0; i < FileDataNum; i++) {
		printf("name: %s\n",FileData[i].name);
		for (s32 ti = 0; ti < FileData[i].TagNum; ti++) {
			printf("tag: %s\n",FileData[i].tags[ti]->tag);
		}
	}*/
	
	/*for (s32 i = 0; i < TagDataNum; i++) {
		printf("tag: %s\n",TagData[i]->tag);
		for (s32 ni = 0; ni < TagData[i]->NameNum; ni++) {
			printf("name: %s\n",TagData[i]->names[ni]->name);
		}
	}*/
}

s32 SlashNumber(char* path) {
	s32 num = 0;
	
	s32 len = strlen(path);
	for (s32 i = 0; i < len; i++) {
		if (path[i] == '/') {
			num++;
		}
	}
	
	return num;
}

char* PathName(char* path) {
	s32 num = SlashNumber(path);
	
	s32 len = strlen(path);
	for (s32 i = 0; i < len; i++) {
		if (path[i] == '/') {
			num--;
		}
		if (num == 0) {
			return &path[i + 1];
		}
	}
	
	return NULL; //must be never reached
}

static int fs_getattr(const char* path,struct stat* stbuf) {
	memset(stbuf,0,sizeof(struct stat));
	
	if (!strcmp(path,"/") || !strcmp(path,"/.")) {
		stbuf->st_mode = S_IFDIR | 0777;
		stbuf->st_nlink = 2;
		return 0;
	}
	else {
		s32 sln = SlashNumber((char*)path);
		
		if (sln == 1) {
			//for (s32 i = 0; i < TagDataNum; i++) {
				//if (!strcmp((char*)dd[tn].tag,(char*)tag)) {
					stbuf->st_mode = S_IFDIR | 0777;
					stbuf->st_nlink = 2;
					return 0;
				//}
			//}
		}
		else if (sln == 2) {
			char* name = PathName((char*)path);
			
			for (s32 i = 0; i < FileDataNum; i++) {
				if (!strcmp((char*)FileData[i].name,name)) {
					//u32 fn;
					//for (fn = 0; fn < fdNum; fn++) {
						//sprintf((char*)namebuf,"/%s/%s",dd[tn].tag,fd[fn].name);
						//if (!strcmp((char*)path,(char*)namebuf)) {
							stbuf->st_mode = S_IFREG | 0444;
							
							s8 namebuf[256];
							
							snprintf((char*)namebuf,256,"%s/%s",FilePath,FileData[i].name);
							s32 fd = open((char*)namebuf,O_RDONLY);
							u32 size = lseek(fd,0,SEEK_END);
							close(fd);
							stbuf->st_size = size;
							return 0;
						//}
					//}
				}
			}
		}
		return -ENOENT;
	}
	
	return 0;
}

static int fs_readdir(const char* path,void* buffer,fuse_fill_dir_t filler,off_t offset,struct fuse_file_info* fi) {
	filler(buffer,".",NULL,0);
	filler(buffer,"..",NULL,0);
	
	if (strcmp(path,"/" ) == 0) {
		for (s32 i = 0; i < TagDataNum; i++) {
			filler(buffer,(char*)TagData[i]->tag,NULL,0);
		}
	}
	else {
		char* name = PathName((char*)path);
		
		for (s32 i = 0; i < TagDataNum; i++) {
			if (!strcmp((char*)TagData[i]->tag,name)) {
				for (s32 fi = 0; fi < TagData[i]->NameNum; fi++) {
					filler(buffer,(char*)TagData[i]->names[fi]->name,NULL,0);
				}
				break;
			}
		}
	}
	
	return 0;
}

static int fs_read(const char* path,char* buffer,size_t size,off_t offset,struct fuse_file_info* fi) {
	char* name = PathName((char*)path);
	
	s8 namebuf[256];
	sprintf((char*)namebuf,"%s/%s",FilePath,name);
	
	s32 fd = open((char*)namebuf,O_RDONLY);
	size_t len = lseek(fd,0,SEEK_END);
	lseek(fd,0,SEEK_SET);
	
	if (offset < len) {
		if (offset + size > len) {
			size = len - offset;
		}
		lseek(fd,offset,SEEK_SET);
		read(fd,buffer,size);
	}
	else {
		size = 0;
	}
	
	close(fd);
	
	return size;
}

static int fs_open(const char* path,struct fuse_file_info* fi) {;
	if ((fi->flags & 3) != O_RDONLY) {
		return -EACCES;
	}
	
	char* name = PathName((char*)path);
	
	s8 namebuf[256];
	snprintf((char*)namebuf,256,"%s/%s",FilePath,name);
	
	s32 fd = open((char*)namebuf,O_RDONLY);
	if (fd == -1) {
		return -ENOENT;
	}
	
	close(fd);
	
	return 0;
}

static struct fuse_operations operations = {
	.readdir =		fs_readdir,
	.open =			fs_open,
	.read =			fs_read,
	.getattr =		fs_getattr,
};

int main(int argc,char* argv[]) {
	if (argc != 4) {
		printf("usage: %s absolute_path_to_files_directory path_to_list absolute_path_to_mount_directory\n",argv[0]);
		exit(1);
	} 
	
	FilePath = (s8*)argv[1];
	ListPath = (s8*)argv[2];
	MountPath = (s8*)argv[3];
	
	FILE* fp = fopen((char*)ListPath,"rb");
	fseek(fp,0,SEEK_END);
	s32 size = ftell(fp);
	fseek(fp,0,SEEK_SET);
	s8* data = malloc(size);
	fread(data,size,1,fp);
	fclose(fp);
	
	ParseList(data,size);
	
	argv[1] = argv[3];
	
	return fuse_main(2,argv,&operations,NULL);
}
