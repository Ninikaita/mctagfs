#!/bin/python

import sys
import os

if (len(sys.argv) != 2):
	print("usage: " + sys.argv[0] + " <directory>")
	quit()
lines = os.listdir(sys.argv[1])
fd = open("list.txt","wb")
fd.write("format 0".encode("utf8"))
fd.write("\n".encode("utf8"))
fd.write("\n".encode("utf8"))
for l in lines:
	fd.write(l.encode("utf8","surrogateescape"))
	fd.write("\n".encode("utf8"))
	fd.write("dummytag".encode("utf8"))
	fd.write("\n".encode("utf8"))
	fd.write("\n".encode("utf8"))
