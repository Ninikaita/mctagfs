## Description

This is simple programm, that creates tag file system from list file and files directory.  

## How to create tag file system

Put in some directory directory "files" and file "list.txt".  
In list.txt write:  
file1  
tag1  
tag2  
tag3  

file2  
tag1  
tag2  
tag3  

file3  
tag1  
tag2  
tag3  

...  

(all file+tag sections must be separated with one newline)  

file1 is file, located at files/file1.  
Belove its tags.  

To "files" put files "file1", "file2", "file3"  

Launch mctagfs:  
mctagfs absolute\_path\_to\_files\_directory path\_to\_list.txt path\_to\_mount\_file\_system  

For unmount use umount  

You can use python script create\_list.py to create simple list.txt:  
./pythonscript/create_list.py path\_to\_files  
This script creates file "list.txt"  



## Future features
0. JSON support.
1. Time when file added.
2. Choose and unchoose multiple tags with subdirectories.
